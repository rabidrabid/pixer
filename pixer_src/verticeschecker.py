from bmesh.types import BMesh
from .logger import log, ERROR
from .utils import almost_equal
from math import floor


def has_duplicates(bm: BMesh):
    vertices = set()
    for vert in bm.verts:
        if vert.co.copy().freeze() in vertices:
            log(ERROR, "The vert " + str(vert.co.copy().freeze()) + " is already in the set, it's probably a duplicate")
        vertices.add(vert.co.copy().freeze())
    if len(bm.verts) != len(vertices):
        return True
    return False


def are_vertices_aligned_to_grid(bm: BMesh, context):
    subdivisions = float(context.space_data.overlay.grid_subdivisions)
    unitf = 1.0 / subdivisions
    for vert in bm.verts:
        x = abs(vert.co.x) / unitf
        y = abs(vert.co.y) / unitf
        z = abs(vert.co.z) / unitf

        x = x - floor(x)
        y = y - floor(y)
        z = z - floor(z)

        is_grid_x = almost_equal(0.0, x) or almost_equal(1.0, x)
        is_grid_y = almost_equal(0.0, y) or almost_equal(1.0, y)
        is_grid_z = almost_equal(0.0, z) or almost_equal(1.0, z)
        if not is_grid_x or not is_grid_y or not is_grid_z:
            return False
    return True
