import bpy


class CopyGridSizeOperator(bpy.types.Operator):
    bl_label = "PixerWarnings"
    bl_idname = "rabid.pixercopygridsize"

    def execute(self, context):
        scene = context.scene
        pixer = scene.pixer
        pixer.pixels_in_3D_unit = context.space_data.overlay.grid_subdivisions
        return {'FINISHED'}
