# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import bpy

from .pixeroperator import PixerOperator
from .warningsoperator import WarningsOperator
from .copygridsizeoperator import CopyGridSizeOperator

bl_info = {
    "name": "Pixer",
    "author": "Rabid",
    "version": (1, 0),
    "blender": (2, 92, 0),
    "description": "Adjusts the UVs from a model to set the textures to pixels",
    "wiki_url": "https://gitlab.com/RabidTunes/pixer",
    "category": "UV"
}


class PixerProperties(bpy.types.PropertyGroup):
    pixels_in_3D_unit: bpy.props.IntProperty(name="Pixels/unit",
                                             description="How many squares are inside a 3D unit (you can use "
                                                         "ortographic view to check this)",
                                             default=10, min=1)
    texture_size: bpy.props.IntProperty(name="Texture Size", description="Size of texture. Assumes it is squared",
                                        default=32, min=1)
    selection_only: bpy.props.BoolProperty(name="Selection only",
                                           description="Check this to apply the texturizer only to "
                                                       "selected faces",
                                           default=False)
    use_seams_as_pixel_align: bpy.props.BoolProperty(name="Use seams as pixel align direction",
                                                     description="Check this to use marked seams in edges as the up "
                                                                 "direction "
                                                                 " that the pixels must follow",
                                                     default=False)
    follow_near_aligned: bpy.props.BoolProperty(name="Follow near aligned",
                                                description="Check this to look near aligned faces in "
                                                            "order to align its own pixels in case they have no aligned"
                                                            " edges",
                                                default=False)

    # Internal for warnings
    all_ok: bpy.props.BoolProperty(name="All ok", default=False)
    check_duplicated_vertices: bpy.props.BoolProperty(name="Check duplicated vertices", default=False)
    grid_pixels_check: bpy.props.BoolProperty(name="Check grid and pixels in 3D unit matching", default=False)
    check_aligned_vertices: bpy.props.BoolProperty(name="Check vertices alignment", default=False)


class PixerMainPanel(bpy.types.Panel):
    bl_label = "Pixer"
    bl_idname = "Pixer"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Pixer"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def draw(self, context: bpy.context):
        layout = self.layout
        scene = context.scene
        pixer = scene.pixer

        if bpy.context.active_object.mode == 'EDIT':
            layout.label(text="Settings", icon="TOOL_SETTINGS")
            settings_box = layout.box()
            settings_box.prop(pixer, "texture_size")
            settings_box.prop(pixer, "pixels_in_3D_unit")
            row = settings_box.row()
            row.operator(text="^ Copy from grid size ^", operator="rabid.pixercopygridsize")

            layout.label(text="Options", icon="PARTICLES")
            options_box = layout.box()
            options_box.prop(pixer, "selection_only")
            options_box.prop(pixer, "follow_near_aligned")
            options_box.prop(pixer, "use_seams_as_pixel_align")

            layout.label(text="Run", icon="GHOST_ENABLED")
            run_box = layout.box()
            row = run_box.row()
            row.operator(text="Check model", operator="rabid.pixerwarnings")

            if pixer.all_ok:
                run_box.label(text="Everything looks good!", icon="CHECKMARK")

            if pixer.check_duplicated_vertices or pixer.grid_pixels_check or pixer.check_aligned_vertices:
                run_box.label(text="- Warnings! -", icon="TRIA_DOWN")
            if pixer.check_duplicated_vertices:
                run_box.label(text="Duplicated vertices detected", icon="ERROR")
            if pixer.grid_pixels_check:
                run_box.label(text="Pixels/unit does not match grid size", icon="QUESTION")
            if pixer.check_aligned_vertices:
                run_box.label(text="Vertices not aligned to grid", icon="QUESTION")
            if pixer.check_duplicated_vertices or pixer.grid_pixels_check or pixer.check_aligned_vertices:
                run_box.label(text="- - - - - - -", icon="TRIA_UP")

            row = run_box.row()
            row.label(icon='KEYTYPE_KEYFRAME_VEC')
            row.operator(text="Pixelize!", operator="rabid.pixer")
            row.label(icon='KEYTYPE_KEYFRAME_VEC')
        else:
            layout.label(text="Pixer only works in Edit mode", icon="ERROR")


class PixerHelpSubPanel(bpy.types.Panel):
    bl_label = "Help"
    bl_idname = "PixerHelp"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Pixer"
    bl_parent_id = "Pixer"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context: bpy.context):
        layout = self.layout
        layout.label(text="Help", icon="HELP")
        row = layout.row()
        row.operator("wm.url_open", text="Go to Pixer wiki").url = "https://gitlab.com/RabidTunes/pixer/-/wikis/home"


classes = [PixerProperties, PixerMainPanel, PixerHelpSubPanel, PixerOperator, WarningsOperator, CopyGridSizeOperator]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.pixer = bpy.props.PointerProperty(type=PixerProperties)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.pixer


if __name__ == "__main__":
    register()
