# [ Pixer v1.0 ] | [Click here to go to releases and download the latest version](https://gitlab.com/RabidTunes/pixer/-/releases)

This is a Blender Addon that makes your model's textures ✨pixel perfect✨

This means that if you apply a pixel art texture the pixels will be of a consistent size across your model.

# HOW TO INSTALL

Download the zip file and install it from preferences.

# HOW TO USE

COMING SOON FOR VERSION 1.0

Video explanation: Coming 🔜
